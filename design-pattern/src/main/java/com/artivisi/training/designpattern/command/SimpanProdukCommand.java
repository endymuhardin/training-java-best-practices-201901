package com.artivisi.training.designpattern.command;

public class SimpanProdukCommand implements Command {
    public void jalankan() {
        System.out.println("Menyimpan data produk");
    }
}
