package com.artivisi.training.designpattern.command;

public class AplikasiService {
    public void simpanProduk() {

    }

    public void simpanTransaksi() {

    }

    // semua fitur diimplement di sini
}
