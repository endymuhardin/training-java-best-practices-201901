package com.artivisi.training.designpattern.builder;

public class Customer {
    private String name;
    private String email;
    private String address;

    public Customer(){}

    // telescopic constructor
    public Customer(String name) {
        this.name = name;
    }

    public Customer(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public Customer(String name, String email, String address) {
        this.name = name;
        this.email = email;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public static CustomerBuilder builder(){
        return new CustomerBuilder();
    }

    static class CustomerBuilder {

        Customer c = new Customer();

        public Customer build() {
            if(c.getName() == null || c.getName().trim().length() < 1) {
                throw new IllegalStateException("Nama tidak boleh null");
            }
            return c;
        }

        public CustomerBuilder name(String nama) {
            c.setName(nama);
            return this;
        }
    }
}

