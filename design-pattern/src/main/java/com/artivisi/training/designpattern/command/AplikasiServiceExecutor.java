package com.artivisi.training.designpattern.command;

public class AplikasiServiceExecutor {
    public void jalankan(Command cmd) {

        // polymorphism
        cmd.jalankan();
    }
}
